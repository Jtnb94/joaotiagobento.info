## ESTRUTURA DAS PASTAS ##

### Páginas ###
* home: [joao-bento.info/home/](http://joao-bento.info/home/)
	* home.html - página principal
* 404: [joao-bento.info/404/](http://joao-bento.info/404/)
	* custom_404.html - página de erro 404
* 50x: [joao-bento.info/50x/](http://joao-bento.info/50x/)
	* custom_50x.html - página de erro 50X (500, 502, 503 e 504)

### Stylesheets ###
* css: ficheiros CSS
	* [base.css](http://joao-bento.info/css/base.css) - CSS geral.
	* [home.css](http://joao-bento.info/css/style.css) - CSS para a página Home.

### Scripts ###
* js: ficheiros JS
	* [home.js](http://joao-bento.info/js/home.js) - JS para a página Home.

### Media ###
* images: repositório de imagens

### Analytics ###
* [Google Analytics](https://analytics.google.com/analytics/web/?authuser=0#home/a87773881w130446561p134359100/) nas páginas:
	* [home](http://joao-bento.info/home/)

## TODO ##

* Colocar e actualizar conteúdo (as always)